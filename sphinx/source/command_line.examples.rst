command\_line.examples package
==============================

Submodules
----------

command\_line.examples.example\_shell module
--------------------------------------------

.. automodule:: command_line.examples.example_shell
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: command_line.examples
   :members:
   :undoc-members:
   :show-inheritance:
