
Description
==============

This is a simple command line library.

We can use it to make writing CLIs easier.

Shell, color printing and typewriter effects are now supported.

This library is short, personal, and free!

Based on LGPL protocol!

Need
====

You only need Python3, and you need the Colorama library.

``pip install colorama``

``pip install commandline_lib``