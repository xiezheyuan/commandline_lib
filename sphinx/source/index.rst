.. Command Line documentation master file, created by
   sphinx-quickstart on Fri Mar 12 19:01:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Command Line's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Description
==========
This is a simple command line library.

We can use it to make writing CLIs easier.

``pip install commandline``


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
