command\_line package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   command_line.examples

Submodules
----------

command\_line.colorful module
-----------------------------

.. automodule:: command_line.colorful
   :members:
   :undoc-members:
   :show-inheritance:

command\_line.exceptions module
-------------------------------

.. automodule:: command_line.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

command\_line.history module
----------------------------

.. automodule:: command_line.history
   :members:
   :undoc-members:
   :show-inheritance:

command\_line.shell module
--------------------------

.. automodule:: command_line.shell
   :members:
   :undoc-members:
   :show-inheritance:

command\_line.style module
--------------------------

.. automodule:: command_line.style
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: command_line
   :members:
   :undoc-members:
   :show-inheritance:
